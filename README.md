# assessment1

First Assessment test

This repository contains files for Docker Multistage build.

Files in the directory

1. Dockerfile
2. certificates


Command to run

docker build --rm --build-arg keypass=<password> -t <image_name>:<tag> .

docker run -d --name <docker_name> <image_name>:<tag> ls -la /usr/share/local/ca-certificates/certificates