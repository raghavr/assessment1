FROM openjdk:9-jdk-slim AS imagebuilder
ARG keypass=$keypass
COPY certificates /usr/local/share/ca-certificates/certificates
RUN apt-get update && apt-get install --no-install-recommends -y -qq ca-certificates-java && \
 update-ca-certificates
RUN groupadd --gid 1000 java &&\
 useradd --uid 1000 --gid java --shell /bin/bash --create-home java && \
 chmod -R a+w /home/java
WORKDIR /home/java

FROM imagebuilder AS stage2
RUN keytool -import -alias certkeystore -file /usr/local/share/ca-certificates/certificates -storetype JKS -keystore server.keystore -storepass $keypass -noprompt -trustcacerts
RUN apt-get clean && apt-get autoclean
#RUN rm -rf server.keystore

FROM stage2 AS final
RUN echo "Cleaning Certificates" && rm -rf server.keystore /usr/local/share/ca-certificates/certificates